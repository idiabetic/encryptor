# Encryptor

Simple ruby project to encrypt and decrypt messages.

This currently uses a array rotation to encrypt messages.
It can currently encrypt and decrypt messages and files.
It can also crack an encryption.

```ruby
# You can encrypt messages using the .encrypt(message, encryption_rotation)
encryptor = Encryptor.new
encrypted_message = encryptor.encrypt('Message!', 13)

# You can decrypt messages using the .decrypt(message, decryption_rotation)
decrypted_message = encryptor.decrypt(encrypted_message, 13)

# Files largely work the same as messages.
# Decrypt - .encrypt_file(file, encryption_rotation)
encryptor.encrypt_file("/path/to/file.txt", 13)
# It will output to encrypted.txt

encryptor.decrypt_file("/path/to/file.txt", 13)
# It will output to decrypted.txt

# Cracking requires the encrypted message, and a range of encryption_rotation
# values.
encryptor.crack('message', 1..15)
```
## Test Suite
The test suite currently tests:
* encryption
* decryption
* Positive & negative cipher rotation
* File Encryption
* File Decryption
* Encryption Crack
