gem 'minitest', '~> 5.2'
require 'minitest/autorun'
require 'minitest/pride'
require_relative 'encryptor'

class EncryporTest < Minitest::Test

  def test_it_encrypts
    encryptor = Encryptor.new
    assert_equal 'uryy!', encryptor.encrypt('hello', 13)
  end

  def test_encryption_rotation_works
    encryptor = Encryptor.new
    assert_equal "'$++.", encryptor.encrypt('hello', 26)
  end

  def test_negative_rotation_works
    encryptor = Encryptor.new
    assert_equal "NKRRU", encryptor.encrypt('hello', -26)
  end

  def test_encrypting_capitalization_works
    encryptor = Encryptor.new
    assert_equal "b$++.", encryptor.encrypt('Hello', 26)
  end

  def test_decryption_works
    encryptor = Encryptor.new
    assert_equal "hello", encryptor.decrypt('uryy!', 13)
  end

  def test_decryption_rotation_works
    encryptor = Encryptor.new
    assert_equal "hello", encryptor.decrypt("'$++.", 26)
  end

  def test_negative_decryption_rotation_works
    encryptor = Encryptor.new
    assert_equal "hello", encryptor.decrypt("NKRRU", -26)
  end

  def test_decrypting_capitalization_works
    encryptor = Encryptor.new
    assert_equal "Hello", encryptor.decrypt('b$++.', 26)
  end

  def test_file_encrypt
    encryptor = Encryptor.new
    encryptor.encrypt_file("single_test.txt", 13)
    assert_equal "auv%-v%-n-&r%&.", File.read('encrypted.txt')
  end

  def test_it_cracks
    encryptor = Encryptor.new
    cracks_hash = encryptor.crack('uryy!', 0..13)
    assert cracks_hash.has_value?('hello')
  end
end
