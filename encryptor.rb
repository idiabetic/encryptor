#!/usr/bin/env ruby -wKU
# encoding: UTF-8


class Encryptor

  def cipher(rotation)
    Hash[((' '..'z').to_a).zip(((' '..'z').to_a).rotate(rotation))]
  end

  def encrypt(string, rotation)
    string.split('').map {|i| cipher(rotation)[i]}.join('')
  end

  def decrypt(string, rotation)
    string.split('').map {|i| cipher(rotation * -1)[i]}.join('')
  end

  def encrypt_file(file, rotation)
    writer = File.open("encrypted.txt", "w")
    writer.write(File.read(file).split('').map {|i| cipher(rotation)[i]}.join(''))
    writer.close
  end

  def decrypt_file(file, rotation)
    writer = File.open("decrypted.txt", "w")
    writer.write(File.read(file).split('').map {|i| cipher(rotation * - 1)[i]}.join(''))
    writer.close
  end

  def crack(encrypted, rotation_range)
    hash = {}
    rotation_range.each do |x|
      hash[x] = encrypted.split('').map {|i| cipher(x * -1)[i]}.join('')
      puts "#{x} : #{hash[x]}"
    end
    return hash
  end
end
